package com.edwardlu.bleapp2;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.AdvertiseData;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "Main";

    // constant for the location permissions request broadcast
    private final int REQUEST_COARSE_LOCATION = 11;

    // constant to define the scanning period
    private static final long SCAN_PERIOD = 10000;

    // bluetoothAdapter to enable bluetooth, start LE scanning
    private BluetoothAdapter bluetoothAdapter;

    // listAdapter to act as bridge between discovered device data and UI's listview
    private ListAdapter mLeDeviceListAdapter;

    // buttons for user interaction
    private Button enableBluetoothButton;
    private Button enableLocationServicesButton;
    private Button startBleScanButton;
    private Button moreInfoForBleDeviceButton;

    // textViews to display selected device information
    private TextView selectedDeviceName;
    private TextView selectedDeviceAddress;
    private TextView scanRecordInfo;

    // help with BLE scanning
    private boolean scanning;
    private Handler handler;

    // ArrayList to hold scanRecord information for discovered devices
    private ArrayList<byte[]> discoveredDevicesScanRecords;

    // ArrayList to hold display information for discovered devices
    private ArrayList<String> discoveredDevices;

    // ArrayList to hold discovered bluetooth device objects
    private ArrayList<BluetoothDevice> discoveredDevicesObjects;

    // HashSet to prevent duplicate entries into the ArrayList
    private HashSet<String> discoveredUniqueDevices;

    // arrayAdapter to display elements of discoveredDevices arrayList
    private ArrayAdapter<String> arrayAdapter;

    // listview for displaying discovered devices
    private ListView discoveredDevicesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initializing handler, which is used to limit the BLE scan to SCAN_PERIOD milliseconds
        handler = new Handler();

        // initializing data structures that hold discovered devices
        discoveredDevices = new ArrayList<String>();
        discoveredUniqueDevices = new HashSet<String>();
        discoveredDevicesObjects = new ArrayList<BluetoothDevice>();
        discoveredDevicesScanRecords = new ArrayList<byte[]>();

        // initializing listView that displays discovered devices
        discoveredDevicesList = (ListView) findViewById(R.id.discoveredDevicesList);

        // getting default bluetooth Adapter for enabling bluetooth / doing LE scan
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // initialize the textviews to hold selected device
        selectedDeviceName = (TextView) findViewById(R.id.deviceName);
        selectedDeviceAddress = (TextView) findViewById(R.id.deviceAddress);
        scanRecordInfo = (TextView) findViewById(R.id.scanRecordInfo);

        if (bluetoothAdapter == null) {
            // some way to exit the program? or notify the user?
        }

        // Turn bluetooth on
        enableBluetoothButton = (Button) findViewById(R.id.enableBluetooth);
        enableBluetoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.enable();
                }
                updateUI();
            }
        });

        // Get the user's permission for location services for the app
        enableLocationServicesButton = (Button) findViewById(R.id.enableLocation);
        enableLocationServicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLocationPermission();
                updateUI();
            }
        });

        // button to scan for BLE devices for a specific amount of time
        startBleScanButton = (Button) findViewById(R.id.scanBLE);
        startBleScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!scanning) {
                    discoveredDevicesScanRecords.clear();
                    discoveredUniqueDevices.clear();
                    discoveredDevices.clear();
                    discoveredDevicesObjects.clear();
                }
                if (scanning) {
                    scanLeDevice(false);
                } else {
                    scanLeDevice(true);
                }
                updateUI();
            }
        });

        // button to get more information about BLE Device
        moreInfoForBleDeviceButton = (Button) findViewById(R.id.moreInfoButton);
        moreInfoForBleDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startConnectIntent = new Intent(MainActivity.this, ConnectActivity.class);

                startConnectIntent.putExtra("DEVICE_INFO",
                        selectedDeviceAddress.getText().toString() + selectedDeviceName.getText().toString());

                startActivity(startConnectIntent);
            }
        });

        // arrayAdapter for dispalying the list of discovered devices on screen
        arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, discoveredDevices);
        // Set The Adapter
        discoveredDevicesList.setAdapter(arrayAdapter);

        // register onClickListener to handle click events on each item
        discoveredDevicesList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            // argument position gives the index of item which is clicked
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                String selectedDevice = discoveredDevices.get(position);
                //Toast.makeText(getApplicationContext(),
                //        "Address of device selected: " + selectedDevice.substring(selectedDevice.lastIndexOf(' ') + 1),
                //        Toast.LENGTH_LONG).show();
                selectedDeviceName.setText(selectedDevice.substring(0, selectedDevice.indexOf('\n')));
                selectedDeviceAddress.setText(selectedDevice.substring(selectedDevice.lastIndexOf(' ') + 1));
                scanRecordInfo.setText(parseScanRecord(discoveredDevicesScanRecords.get(position)));

                updateUI();


            }
        });

        // update UI so buttons have correct text, devices are properly displayed, etc
        updateUI();
    }

    // Stops scanning after SCAN_PERIOD milliseconds to save battery
    private void scanLeDevice ( final boolean enable){
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scanning = false;
                    bluetoothAdapter.stopLeScan(mLeScanCallback);
                    Log.d(TAG, "stopped LE scan after timer ran out");
                    updateUI();
                }
            }, SCAN_PERIOD);

            scanning = true;
            bluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            scanning = false;
            bluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        updateUI();
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     final byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String deviceInfo = "Device name: " + device.getName() + "\nDevice Address:\n " + device.getAddress();
                            if (!discoveredUniqueDevices.contains(deviceInfo)) {

                                discoveredDevicesScanRecords.add(scanRecord);
                                discoveredDevicesObjects.add(device);
                                discoveredUniqueDevices.add(deviceInfo);
                                arrayAdapter.add(deviceInfo);

                            }
                        }
                    });
                }
            };

    // this function is called throughout the program to update the UI; change button text, etc
    void updateUI() {
        enableBluetoothButton.setText(
                bluetoothAdapter.isEnabled() ? "Bluetooth Already Enabled" : "Enable Bluetooth"
        );

        enableLocationServicesButton.setText(
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED ? "Location Services Already Enabled" : "Enable Location Services"
        );

        startBleScanButton.setText(
                scanning ? "Cancel Current Scan" : "Scan for BLE Devices"
        );

        if (selectedDeviceName.getText().toString().equals("No Device Selected")) {
            moreInfoForBleDeviceButton.setEnabled(false);
        } else {
            moreInfoForBleDeviceButton.setEnabled(true);
        }

    }

    // function for requesting location services permission from the user
    protected boolean requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "We didn't have the user's permission to use location services yet...");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COARSE_LOCATION);
        } else {
            Log.d(TAG, "We DID have the user's permission to use location services");
            return true;
        }

        return false;
    }

    // function that checks the result of asking the user for location services permission
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "We got the user's permission to do location services....");
                } else {
                    Log.d(TAG, "We didn't get user's permission to do location services...");
                }
                break;
            }
        }
    }

    // overrode callback so that if the user leaves app and disables bluetooth or something else,
    // the ui can be updated properly
    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "We resumed the activity");

        updateUI();
    }

    // Bluetooth Spec V4.0 - Vol 3, Part C, section 8
    private static String parseScanRecord(byte[] scanRecord) {
        StringBuilder output = new StringBuilder();
        int i = 0;
        while (i < scanRecord.length) {
            int len = scanRecord[i++] & 0xFF;
            if (len == 0) break;
            switch (scanRecord[i] & 0xFF) {
                case 0x01:
                    output.append("\n «Flags»: ")
                    .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x02:
                    output.append("\n «Incomplete List of 16-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x03:
                    output.append("\n «Complete List of 16-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x04:
                    output.append("\n «Incomplete List of 32-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x05:
                    output.append("\n «Complete List of 32-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x06:
                    output.append("\n «Incomplete List of 128-bit Service Class UUIDs»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x07: // list of uuid's
                    output.append("\n «Complete List of 128-bit Service Class UUIDs»: ")
                    .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x08:
                    output.append("\n «Shortened Local Name»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                case 0x09:
                    output.append("\n «Complete Local Name»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i+1, len));
                    break;
                // https://www.bluetooth.org/en-us/specification/assigned-numbers/generic-access-profile
                case 0x0A: // Tx Power
                    output.append("\n «Tx Power Level»: ").append(scanRecord[i+1]);
                    break;
                case 0xFF: // Manufacturer Specific data (RFduinoBLE.advertisementData)
                    output.append("\n «Manufacturer Specific Data»: ")
                            .append(HexAsciiHelper.bytesToHex(scanRecord, i + 3, len));

                    String ascii = HexAsciiHelper.bytesToAsciiMaybe(scanRecord, i + 3, len);
                    if (ascii != null) {
                        output.append(" (\"").append(ascii).append("\")");
                    }
                    break;
            }
            i += len;
        }
        return output.toString();
    }

}
